//
//  ViewController.swift
//  SpeechRec
//
//  Created by uasi on 2021/12/18.
//

import UIKit

import Speech
import AVFoundation
import AudioToolbox

class ViewController: UIViewController {
    @IBOutlet var micButton: UIButton!
    @IBOutlet var speechButton: UIButton!

    private let speechRecognizer = SFSpeechRecognizer(locale: Locale(identifier: "ja-JP"))!
    private var recognitionRequest: SFSpeechAudioBufferRecognitionRequest?
    private var recognitionTask: SFSpeechRecognitionTask?
    private let audioEngine = AVAudioEngine()

    private let speech = AVSpeechSynthesizer()

    override func viewDidLoad() {
        super.viewDidLoad()

        micButton.addTarget(self, action: #selector(self.recordButtonTapped(sender:)), for: .touchUpInside)
        micButton.setTitle("Start Recording", for: [])
        micButton.isEnabled = false

        speechButton.addTarget(self, action: #selector(self.speechButtonTapped(sender:)), for: .touchUpInside)

        speechRecognizer.delegate = self

        SFSpeechRecognizer.requestAuthorization { authStatus in

            OperationQueue.main.addOperation {
                switch authStatus {
                case .authorized:
                    // ユーザが音声認識の許可を出した時
                    self.micButton.isEnabled = true

                case .denied:
                    // ユーザが音声認識を拒否した時
                    self.micButton.isEnabled = false
                    self.micButton.setTitle("User denied access to speech recognition", for: .disabled)

                case .restricted:
                    // 端末が音声認識に対応していない場合
                    self.micButton.isEnabled = false
                    self.micButton.setTitle("Speech recognition restricted on this device", for: .disabled)

                case .notDetermined:
                    // ユーザが音声認識をまだ認証していない時
                    self.micButton.isEnabled = false
                    self.micButton.setTitle("Speech recognition not yet authorized", for: .disabled)

                default:
                    print("音声認識認証エラー")
                }
            }
        }
    }

    @objc func speechButtonTapped(sender: UIButton) {
        print("スピーチボタンがタップされた")

        let speechUtterance = AVSpeechUtterance(string: "Hello!")
        speech.speak(speechUtterance)
    }

    @objc func recordButtonTapped(sender: UIButton) {
        print("録音ボタンがタップされた")

        if audioEngine.isRunning {
            audioEngine.stop()
            recognitionRequest?.endAudio()
            micButton.isEnabled = false
            micButton.setTitle("Stopping", for: .disabled)

            // 録音が停止した！
            print("録音停止")

        } else {
            try? startRecording()
            micButton.setTitle("Stop recording", for: [])

        }
    }

    // 録音を開始する
    private func startRecording() throws {
        print("録音開始")

        // Cancel the previous task if it's running.
        if let recognitionTask = recognitionTask {
            recognitionTask.cancel()
            self.recognitionTask = nil
        }

        let audioSession = AVAudioSession.sharedInstance()
        try audioSession.setCategory(AVAudioSession.Category.record)
        try audioSession.setMode(AVAudioSession.Mode.measurement)
        try audioSession.setActive(true, options: .notifyOthersOnDeactivation)

        print("セッション初期化完了")

        recognitionRequest = SFSpeechAudioBufferRecognitionRequest()

        let inputNode = audioEngine.inputNode
        guard let recognitionRequest = recognitionRequest else { fatalError("Unable to created a SFSpeechAudioBufferRecognitionRequest object") }

        print("録音タスク開始")

        recognitionRequest.shouldReportPartialResults = true
        recognitionTask = speechRecognizer.recognitionTask(with: recognitionRequest) { result, error in
            var isFinal = false

            if let result = result {
                // 音声認識の区切りの良いところで実行される。
                print("RESULT: \(result.bestTranscription.formattedString)")
                isFinal = result.isFinal
            }

            if error != nil || isFinal {
                self.audioEngine.stop()
                inputNode.removeTap(onBus: 0)

                self.recognitionRequest = nil
                self.recognitionTask = nil

                self.micButton.isEnabled = true
                self.micButton.setTitle("Start Recording", for: [])
            }
        }

        let recordingFormat = inputNode.outputFormat(forBus: 0)
        inputNode.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { (buffer: AVAudioPCMBuffer, when: AVAudioTime) in
            self.recognitionRequest?.append(buffer)
        }

        audioEngine.prepare()

        try audioEngine.start()
    }

    // MARK: SFSpeechRecognizerDelegate
    // speechRecognizerが使用可能かどうかでボタンのisEnabledを変更する
    public func speechRecognizer(_ speechRecognizer: SFSpeechRecognizer, availabilityDidChange available: Bool) {
        if available {
            micButton.isEnabled = true
            micButton.setTitle("Start Recording", for: [])

        } else {
            micButton.isEnabled = false
            micButton.setTitle("Recognition not available", for: .disabled)
        }
    }
}

// マイクに関する処理
extension ViewController: SFSpeechRecognizerDelegate {
    // 認証の処理（ここで関数が呼び出されている）

    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
}
